import numpy
import heapq
import pickle

maxDistance = 1000000
alpha = 0.2 
beta = 10 #in minutes
#Convert all times to minutes

# ------ Loading dictionary for time to trip query -------

def load_obj(name ):
    with open('obj/' + name + '.pkl', 'rb') as f:
        return pickle.load(f)
        
sourceTimeDestination = load_obj("TrajData")

# ------------------------------------------------------------

ifile = open('beijing_adj_fromhistory_new.txt')

lines = ifile.readlines()

l1 = lines[0].split(',')
num_nodes = int(l1[0])
num_edges = int(l1[1])

nodes_to_edges_dic = {}
nodes_list = []
adj_list = {}
for i in range(1,num_nodes+1):
	node_line = lines[i].split(',')
	nodes_list.append(int(node_line[0]))
	nodes_to_edges_dic[int(node_line[0])] = []
	for j in range(3, len(node_line)):
		nodes_to_edges_dic[int(node_line[0])].append(node_line[j])
	adj_list[int(node_line[0])] = []

edges_to_nodes_dic = {}
edges_list = []
full_edges_list = []
edge_weight = {}


for i in range(num_nodes+1,len(lines)):
	edge_line = lines[i].split(',')	
	edges_list.append(int(edge_line[0]))
	edges_to_nodes_dic[int(edge_line[0])] = (int(edge_line[1]),int(edge_line[2]),float(edge_line[3]))
	full_edges_list.append( (int(edge_line[1]),int(edge_line[2])) )
	# G_dist.add_edge(int(edge_line[1]),int(edge_line[2]),weight = float(edge_line[3]) )
	edge_weight[(int(edge_line[1]),int(edge_line[2]))] = float(edge_line[3])
	edge_weight[(int(edge_line[2]),int(edge_line[1]))] = float(edge_line[3]) #for undirected only
	adj_list[int(edge_line[1])].append(int(edge_line[2]))
	adj_list[int(edge_line[2])].append(int(edge_line[1])) #for undirected only

active_nodes = []
counter = 0
for node in nodes_list:
	try:
		if(len(sourceTimeDestination[node].keys()) > 0):
			active_nodes.append(node)
			for key in sourceTimeDestination[node].keys():
				active_nodes += sourceTimeDestination[node][key]
	except:
		continue

active_nodes = list(set(active_nodes))
dijkstra_lengths = {}
dijkstra_path = {}
for i in xrange(0, len(nodes_list)):
	try:
		dijkstra_path[ nodes_list[i] ] = {}
		dijkstra_lengths[ nodes_list[i] ] = {}
		for j in xrange(0, len(nodes_list)):
			try:
				dijkstra_lengths[nodes_list[i]][nodes_list[j]] = maxDistance
			except: 
				continue
	except:
		continue
		
def dijkstra(source):
	global nodes_list, adj_list, edge_weight, dijkstra_path, dijkstra_lengths
	heap = []
	tempDis = {}
	for i in xrange(0, len(nodes_list)):
		if(nodes_list[i] in active_nodes): 
			tempDis[ nodes_list[i] ] = maxDistance
	heapq.heappush(heap, (0, (source,[source])))
	tempDis[source][source] = 0
	dijkstra_lengths[source][source] = 0
	dijkstra_path[source][source] = [source]
	while 1:
		top = heapq.heappop(heap)
		if(len(heap) == 0):
			break
		for key in adj_list[top].keys():
			weightNode = tempDis[source][top[1][0]]
			weightKey = tempDis[source][key]
			newWeightKey = weightNode + edge_weight[source][key]
			if( newWeightKey< weightKey):
				tempDis[key] = newWeightKey
				if(key in active_nodes):
					dijkstra_lengths[source][key] = newWeightKey	
					dijkstra_path[source][key] = copy.deepcopy(top[1][1]).append(key)
				headpq.heappush(heap, (-newWeightKey, (key, dijkstra_path[source][key])) )

for i in active_nodes:
	print i
	dijkstra(i)

fp1 = open('all_pairs_shortest_paths.p', 'w')
fp2 = open('all_pairs_shortest_lengths.p', 'w')
pickle.dump(dijkstra_path, fp1)
pickle.dump(dijkstra_lengths, fp2)
fp1.close()
fp2.close()