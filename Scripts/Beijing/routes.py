import os
import pickle

pathFiles = './TrajData/'
listing = os.listdir(pathFiles)
pointsRequired = 1000
trajectoryList = []

counter = 0

for infile in listing:
	file1 =  open(pathFiles+infile, 'r')
	if(counter == pointsRequired):
		break

	while 1:
		line1 = file1.readline()
		line2 = file1.readline()
		line2 = line2[1:-1].split('][')
		if((counter == pointsRequired) or (line1 == "")):
			break
		path = [0]*len(line2) 
		for i in xrange(0, len(path)):
			temp = line2[i].split(',')
			path[i] = int(temp[0])
		counter += 1
		trajectoryList.append(path)

print trajectoryList
