#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <fstream>
#include <unistd.h>
#include <map>
#include <vector>

using namespace std;

string path = "./TrajData/";

map< pair<int,int>, vector<int> > sourceToDestination; 

int getTime(string s)
{
	stringstream ss;
 	ss<<s;
 	int hour, minute;
 	char colon;
 	ss>>hour>>colon>>minute;
	return (hour*60 + minute)/15;
}

int main()
{
	/* Test for function getTime; Output should be 63 */
	// cout<<getTime("008-02-02 15:51:57")<<endl;
	
	system("sh 1.sh");
	usleep(1000);
	ifstream file;
  	file.open("listOfFiles");
  	while(file)
  	{
  		string line;
  		getline(file, line);
  		ifstream trajFile;
  		trajFile.open((path+line).c_str());
  		
  		if(line.substr(0,3) != "bei")
  		{	continue;
  		}

  		while(trajFile)
  		{	string line1, line2;
  			getline(trajFile, line1);
  			getline(trajFile, line2);
  			if(line1 == "")
  				break;
  			string delimiter = "][";

			size_t pos = 0;
			std::string token1, token2;
			pos = line2.find(delimiter);
			int lengthLine2 = line2.length();
  			int posLast = 0;
  			for(int i = lengthLine2-1; ;i--)
  			{	if(line2[i] == '[')
  				{	posLast = i;
  					break;
  				}
  			}
  			token1 = line2.substr(1, pos - 1);
  			token2 = line2.substr(posLast + 1, lengthLine2 - posLast - 2);
  			int source,destination;
  			stringstream ss1(token1);
  			stringstream ss2(token2);
  			ss1>>source; ss2>>destination;
  			char dum1; float dum2; string timeData;
  			ss1>>dum1>>dum2;
  			ss1>>dum1>>dum2; 
  			ss1>>dum1; 
  			ss1>>timeData>>timeData;
  			cout<<source<<" "<<destination<<" "<<getTime(timeData)<<endl;
  			sourceToDestination[make_pair(source, getTime(timeData))].push_back(destination);
  		}

  		break;
  	}
  	file.close();
	

	return 0;
}