import numpy
import copy
import os
import pickle
import Queue

alpha = 0.3
beta = 30 #in minutes
#Convert all times to minutes

fp_read = open('all_pairs_shortest_lengths.p')
shortest_lengths = pickle.load(fp_read)

print "here"
# ------------Abhishek---------------------------------
def parseTimeFromString(s):
	components = s.split(':')
	return (int(components[0])*60 + int(components[1]))/15

def getMeTime(s):
	return parseTimeFromString(s.split(" ")[1])

class node:
    nodeId = 0
    time = 0
    def nodeInit(self):
	    nodeId = 0
	    time = 0

# ------ Loading dictionary for time to trip query -------

def load_obj(name ):
    with open('obj/' + name + '.pkl', 'rb') as f:
        return pickle.load(f)
        
sourceTimeDestination = load_obj("TrajData")

# ---------------------------------------------------------
ifile = open('beijing_adj_fromhistory_new.txt')

lines = ifile.readlines()

l1 = lines[0].split(',')
num_nodes = int(l1[0])
num_edges = int(l1[1])

nodes_to_edges_dic = {}
nodes_list = []
adj_list = {}
for i in range(1,num_nodes+1):
	node_line = lines[i].split(',')
	nodes_list.append(int(node_line[0]))
	nodes_to_edges_dic[int(node_line[0])] = []
	for j in range(3, len(node_line)):
		nodes_to_edges_dic[int(node_line[0])].append(node_line[j])
	adj_list[int(node_line[0])] = []

edges_to_nodes_dic = {}
edges_list = []
full_edges_list = []
edge_weight = {}

# G_dist = nx.Graph()
# G_dist.add_nodes_from(nodes_list)

for i in range(num_nodes+1,len(lines)):
	edge_line = lines[i].split(',')	
	edges_list.append(int(edge_line[0]))
	edges_to_nodes_dic[int(edge_line[0])] = (int(edge_line[1]),int(edge_line[2]),float(edge_line[3]))
	full_edges_list.append( (int(edge_line[1]),int(edge_line[2])) )
	# G_dist.add_edge(int(edge_line[1]),int(edge_line[2]),weight = float(edge_line[3]) )
	edge_weight[(int(edge_line[1]),int(edge_line[2]))] = float(edge_line[3])
	edge_weight[(int(edge_line[2]),int(edge_line[1]))] = float(edge_line[3]) #for undirected only
	adj_list[int(edge_line[1])].append(int(edge_line[2]))
	adj_list[int(edge_line[2])].append(int(edge_line[1])) #for undirected only

class request:
	city = 'beijing'
	def __init__(self,source,destination,req_time):
		self.source = source
		self.destination = destination
		self.start_time = req_time # For now request time is equated with pickup time

def get_all_trips(source, start_time, window):
	global sourceTimeDestination
	tripList = []
	for i in xrange(0, window/15 + 1):
		if source in sourceTimeDestination:
			if (start_time + i) in sourceTimeDestination[source]:
				tripList += copy.deepcopy(sourceTimeDestination[source][start_time + i])
	return tripList

def get_weight(S,D,V,start_time,Tsd):
  score = 0
  Te = 0
  V0 = S
  try:
  	Te = Te + edge_weight[(V0,V)]
  except:
  	Te = Te + 0
  dest_list = get_all_trips(V,start_time/15, beta) #undefined right now
  for W in dest_list:
    Tsv = Te
    try:
      Tvw = shortest_lengths[V][W]
    except:
      Tvw = 0
    try:
  	  Twd = shortest_lengths[W][D]
    except:
      Twd = 0
    if((Tsv + Tvw + Twd) < ((1+alpha)*Tsd)):
  		score +=1
    else:
      try:
        Tvd = shortest_lengths[V][D]
      except:
        Tvd = 0
      try:
  		 Tdw = shortest_lengths[D][W]
      except:
        Tdw = 0
      if((Tvd + Tdw) < ((1+alpha)*Tvw)):
  			score +=1
  return score
		

def dijkstra_lengths(S,D):
  dist = {}
  path = {}
  vset = set()
  nodes = set(nodes_list)
  for node in nodes_list:
    dist[node] = 1000000
    path[node] = -1
    vset.add(node)

  dist[S] = 0
  while( len(vset) > 0):
    x = 1000000
    for node in vset:
      if(dist[node]<x):
        x = dist[node]
        u = node
    if(x == 1000000):
      break
    vset.remove(u)
    for v in adj_list[u]:
      if(v in vset):
        alt = dist[u] + edge_weight[(u,v)]
        if(alt<dist[v]):
          dist[v] = alt
          path[v] = u

  sp = []
  x = D

  while(1):
    sp.append(x)
    if(x==S):
      break
    x = path[x]
  sp.append(S)
  return sp,dist


def dijkstra_pscore(S,D,weights,lim,sld):
  dist = {}
  path = {}
  abs_dist = {}
  vset = set()
  nodes = set(nodes_list)
  for node in nodes_list:
    dist[node] = 1000000
    abs_dist[node] = 1000000
    path[node] = -1
    vset.add(node)
  dist[S] = 0
  abs_dist[S] = 0
  while(len(vset)>0):
    x = 1000000
    for node in vset:
      if(dist[node]<=x):
        x = dist[node]
        u = node
    if(x==1000000):
      break
    try:
      vset.remove(u)
    except:
      break
    if( abs_dist[u] + sld[u] > lim ):
      continue
    for v in adj_list[u]:
      if(v in vset):
        alt = ( dist[u] + edge_weight[(u,v)] + 1 )/ ( float(weights[v]) + 1 )
        if(alt<dist[v] ): #for forward heuristics,add: and sld[u]>=sld[v]
          dist[v] = alt
          abs_dist[v] = abs_dist[u] + edge_weight[(u,v)]
          path[v] = u

  sp = []
  x = D
  try:
    while(1):
    	sp.append(x)
    	if(x==S):
    		break
    	x = path[x]
    sp.append(S)
    sp.reverse()
  except:
    sp = []
  return sp


def DAG_pscore(S, D, weights, lim, sld, distancefromS):
  global edge_weight

  node_sld_pairs = []
  for node in nodes_list:
    if(distancefromS[node] + sld[node] <= lim):
        node_sld_pairs.append((-sld[node],node))

  DAG_list = sorted(node_sld_pairs)
  
  scores = {}
  parent = {}
  for node in nodes_list:
    if(distancefromS[node] + sld[node] <= lim):
      scores[node] = {}
      parent[node] = {}

  scores[S][weights[S]] = 0
  parent[S][weights[S]] = (-1, -1)
  
  si = 0
  for i in range(len(DAG_list)):
    if(DAG_list[i][1] == S):
      si = i
      break
  print si

  for i in range(si, len(DAG_list)):
    u = DAG_list[i][1]
    
    for v in adj_list[u]:
      if( v not in scores):
        continue

      # some nodes are ajacent to themselves in the graph
      if(u == v):  
        continue

      if(sld[v] <= sld[u]):
        for key in scores[u]: 
          if( (key + weights[v] not in scores[v]) ):
            if(scores[u][key] + edge_weight[(u,v)] > lim):           
              continue
            scores[v][key + weights[v]] = 100000
          if( (scores[v][key + weights[v]] > scores[u][key] + edge_weight[(u,v)]) ):
            scores[v][key + weights[v]] = scores[u][key] + edge_weight[(u,v)]
            parent[v][key + weights[v]] = (u, key)

  print scores
  sp = []
  x = D

  # print "Scores at D"  
  # print scores[D].keys()

  numKeys = len(scores[D].keys())
  key = scores[D].keys()[numKeys-1]
  
  while(1):
    # print x, key
    sp.append(x)
    if(x == S):
      break
    (x, key) = parent[x][key]
    
  sp.append(S)
  sp.reverse()
  
  # print sp
  
  return sp


def potential_score(req_ob):
  S = req_ob.source
  D = req_ob.destination
  start_time = req_ob.start_time
  sp = []
  sp,sld = dijkstra_lengths(D, S)
  sp2,sld2 = dijkstra_lengths(S, D)
  
  print sp
  print sp2
  # try:
  #   spl = shortest_lengths[S][D]
  # except:
  #   spl =0
  spl =  sld[S]
  print spl

  weights = {}
  total = 0
  sp_score = 0

  for node in nodes_list:
    weights[node] = get_weight(S, D, node, start_time, spl)
    if(node in sp):
      sp_score += weights[node]
  # np = dijkstra_pscore(S,D,weights,(1+alpha)*spl,sld)
  np = DAG_pscore(S, D, weights, float(1+alpha)*spl, sld, sld2)
  np_score = 0
  npl = 0
  for i in range(len(np)-1):
    np_score += weights[np[i]]
    try:
  	  npl += edge_weight[(np[i],np[i+1])]
    except:
      npl+=0
  if(len(np)>0):
    np_score += weights[np[len(np)-1]]
  try:
  	return np_score,sp_score,npl/spl
  except:
  	return np_score,sp_score,-1


score = 0
numberTrips = 0
i = 0 
total_scores = []
sp_scores = []
np_scores = []
ofile = open('dump_30_dag_heuristics.txt','w')

for id in sourceTimeDestination.keys():
  for time in sourceTimeDestination[id].keys():
    for node in sourceTimeDestination[id][time]:
      i = i + 1
      objectReq = request(id, node, time) 
      print  id,node,time
      (n,s,r) = potential_score(objectReq)
      print n,s,r
      print >>ofile,n,s,r
      np_scores.append(n)
      sp_scores.append(s)
      if(i==100):
       break
    if(i==100):
      break
  if(i==100):
    break
			

# np_scores = numpy.array(total_scores)
# sp_scores = numpy.array(sp_scores)
# numpy.save(open('trajectory_potential_np_scores_90.p','w'),np_scores)
# numpy.save(open('trajectory_sp_scores_90.p','w'),sp_scores)







