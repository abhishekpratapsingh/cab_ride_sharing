
// sizeGraph needed; 
pair<int, vector<int> > dijkstra_lengths(int S, int D):
{ map<int, float> dist;
  map<int, int> path;
  
  for(int i=0; i< sizeGraph; i++)
  { dist[graph[i]] = 1000000;
    path[graph[i]] = -1;
  }

  dist[S] = 0;
  priority_queue< pair<float, int> > dj;
  dj.push(0, S);

  while(dj.size())
  { 
    pair<float, int> x = dj.top();
    dj.pop();
    float dist = x.first;
    int u = x.second;
    for(int i=0; i<adj_list[u].size(); i++)
    { int v = adj_list[u][i];
      float alt = dist[u] + edge_weight[(u,v)];
      if(alt < dist[v])
      { dist[v] = alt; path[v] = u;
        dj.push(make_pair(alt, v));
      }
    }
  }

  vector<int> path;
  int x = D;
  while(1):
  { sp.push_back(x);
    if(x==S)
      break;
    x = path[x];
  }
  sp.push_back(S);

  return make_pair(dist[D], sp);
}